#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  SimiPlot: creating figures showing overall similarity between genomes                                     #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2022 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Bioinformatics and Biostatistics Hub                              research.pasteur.fr/en/team/hub-giphy  #
#   Dpt. Biologie Computationnelle            research.pasteur.fr/team/bioinformatics-and-biostatistics-hub  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.1.230220ac                                                                                       #
# + updating finalizers for BLAST+ version >= 2.13.0                                                         #
# + fixed bug when the reference contains multiple sequences                                                 #
#                                                                                                            #
# VERSION=1.0.220321ac                                                                                       #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = INSTALLATION =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
#  Just give the execute permission to the script SimiPlot.sh with the following command line:               #
#                                                                                                            #
#   chmod +x SimiPlot.sh                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ==============================                                                                             #
# = STATIC FILES AND CONSTANTS =                                                                             #
# ==============================                                                                             #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- constants --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
# COLORS=( "#e6194b" "#3cb44b" "#4363d8" "#f58231" "#911eb4" "#46f0f0" "#f032e6" "#bcf60c" "#fabebe" "#008080" "#e6beff" "#9a6324" "#fffac8" "#800000" "#aaffc3" "#808000" "#ffd8b1" "#000075" "#808080" "#ffffff" "#000000" "#ffe119" );
# COLORS=( "#e31a1c" "#1f78b4" "#6a3d9a" "#33a02c" "#ff7f00" "#b15928" "#fb9a99" "#b2df8a" "#a6cee3" "#fdbf6f" "#cab2d6" "#ffff99" );
# COLORS=( "#e41a1c" "#377eb8" "#4daf4"a "#984ea3" "#ff7f00" "#ffff33" "#a65628" "#f781bf" "#999999" );
#                                                                                                            #
#        red       blue      orange    green     gray      brown     darkgreen pink      clearblue oranged    sand      violet    gold      chocolate darktomato lime      myrtille darkdarkgreen tomato    wine
COLORS=( "#be0032" "#0067a5" "#f38400" "#8db600" "#848482" "#882d17" "#008856" "#e68fac" "#a1caf1" "#f6a600" "#c2b280" "#604e97" "#f3c300" "#654522" "#e25822"  "#dcd300" "#875692" "#2b3d26"     "#f99379" "#b3446c" );
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = FUNCTIONS    =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
# = echoxit() ============================================================================================   #
#   prints in stderr the specified error message $1 and next exit 1                                          #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# = randfile =============================================================================================   #
#   creates and returns a random file name that does not exist from the specified basename $1                #
#                                                                                                            #
randfile() {
  local rdf="$(mktemp $1.XXXXXXXXX)";
  echo $rdf ;
}
#                                                                                                            #
# = mandoc() =============================================================================================   #
#   prints the doc                                                                                           #
#                                                                                                            #
mandoc() {
  cat <<EOF

 USAGE:  SimiPlot  -r <reffile>  -o <outfile>  [options]  <fasta1>  [<fasta2> ...]

 OPTIONS:
  -r <file>    FASTA file containing the reference sequence (mandatory)
  -o <file>    SVG output file name (mandatory)
  -w <int>     window size (default: auto)
  -s           smoothing step (default: not set)
  -x <int>     x-axis start (default: 0)
  -X <int>     x-axis end (default: reference length)
  -y <int>     y-axis start (default: 0)
  -Y <int>     y-axis end (default: 100)
  -d <int>     dot size factor (default: 1.0)
  -a <real>    aspect ratio (detault: 3.0)
  -t <int>     number of threads (default: 2)
  -h           prints this help and exits

EOF
}
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  [ ! $(command -v $GAWK_BIN) ] && echoxit "no $GAWK_BIN detected" ;
  GAWK_STATIC_OPTIONS="";     
  GAWK="$GAWK_BIN $GAWK_STATIC_OPTIONS";
  BAWK="$GAWK -F\" \"";
  TAWK="$GAWK -F\\t";
#                                                                                                            #
# -- NCBI BLAST+ (version >= 2.12.0) ----------------------------------------------------------------------  #
#                                                                                                            #
  export BLAST_USAGE_REPORT=false;         ## from https://www.ncbi.nlm.nih.gov/books/NBK569851/
#                                                                                                            #
# -- makeblastdb                                                                                             #
#                                                                                                            #
  MKBDB_BIN=makeblastdb;
  [ ! $(command -v $MKBDB_BIN) ] && echoxit "no $MKBDB_BIN detected" ;
  version="$($MKBDB_BIN -version | $GAWK '(NR==2){print$3}' | $GAWK -F"." '($1>=2&&$2>=12)')";
  [ -z "$version" ] && echoxit "incorrect version: $MKBDB_BIN" ;
  MKBDB_STATIC_OPTIONS="-input_type fasta";     
  MKBDB="$MKBDB_BIN $MKBDB_STATIC_OPTIONS";
  MKBNDB="$MKBDB -dbtype nucl";
#                                                                                                            #
# -- BLAST                                                                                                   #
#                                                                                                            #
  BLAST_STATIC_OPTIONS="-evalue 0.5 -soft_masking false -max_target_seqs 1 -subject_besthit -max_hsps 1";
#                                                                                                            #
# -- blastn                                                                                                  #
#                                                                                                            #
  BLASTN_BIN=blastn;
  [ ! $(command -v $BLASTN_BIN) ] && echoxit "no $BLASTN_BIN detected" ;
  version="$($BLASTN_BIN -version | $GAWK '(NR==2){print$3}' | $GAWK -F"." '($1>=2&&$2>=12)')";
  [ -z "$version" ] && echoxit "incorrect version: $BLASTN_BIN" ;
  BLASTN_STATIC_OPTIONS="-task blastn -dust no";
  BCOST="-reward 1 -penalty -1 -gapopen 5 -gapextend 2 -xdrop_ungap 20 -xdrop_gap 150 -xdrop_gap_final 100";
  BLASTN="$BLASTN_BIN $BLAST_STATIC_OPTIONS $BLASTN_STATIC_OPTIONS $BCOST";
#                                                                                                            #
##############################################################################################################


##############################################################################################################
####                                                                                                      ####
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                          ####
####                                                                                                      ####
##############################################################################################################

echo -e "\n\033[1m SimiPlot v$VERSION            $COPYRIGHT\033[0m";

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

export LC_ALL=C ;

REFFILE="$NA";       # -r
OUTFILE="$NA";       # -o
WSIZE="$NA";         # -w
SMOOTH=false;        # -s
XMIN="$NA";          # -x
XMAX="$NA";          # -X
YMIN="$NA";          # -y
YMAX="$NA";          # -Y
DOTSPAN=1.0;         # -d
RATIO=3.0;           # -a
NTHREADS=2;          # -t

while getopts r:o:w:x:X:y:Y:d:a:t:sh option
do
  case $option in
  r)  REFFILE="$OPTARG"  ;;
  o)  OUTFILE="$OPTARG"  ;;
  w)  WSIZE=$OPTARG      ;;
  s)  SMOOTH=true        ;;
  x)  XMIN=$OPTARG       ;;
  X)  XMAX=$OPTARG       ;;
  y)  YMIN=$OPTARG       ;;
  Y)  YMAX=$OPTARG       ;;
  d)  DOTSPAN=$OPTARG;   ;;
  a)  RATIO=$OPTARG      ;;
  t)  NTHREADS=$OPTARG   ;;
  h)  mandoc ;  exit 0   ;;
  :)  mandoc ;  exit 1   ;;
  \?) mandoc ;  exit 1   ;;
  esac
done

[ "$REFFILE" == "$NA" ]             && echoxit "reference file not specified (option -r)" ;
[ ! -e $REFFILE ]                   && echoxit "file not found: $REFFILE" ;
[   -d $REFFILE ]                   && echoxit "not a file: $REFFILE" ;
[ ! -s $REFFILE ]                   && echoxit "empty file: $REFFILE" ;
[ ! -r $REFFILE ]                   && echoxit "no read permission: $REFFILE" ;
[ "$OUTFILE" == "$NA" ]             && echoxit "outfile not specified (option -o)" ;
if [ "$WSIZE" != "$NA" ]
then
  [[ $WSIZE =~ ^[0-9]+$ ]]          || echoxit "incorrect value (option -w): $WSIZE" ; 
  [ $WSIZE -lt 30 ]                 && echoxit "window size should at least 30 (option -w)" ;
  [ $WSIZE -gt 50000 ]              && echoxit "window size should at most 50000 (option -w)" ;
fi
if [ "$XMIN" != "$NA" ]
then
  [[ $XMIN =~ ^[0-9]+$ ]]           || echoxit "incorrect value (option -x): $XMIN" ; 
fi
if [ "$XMAX" != "$NA" ]
then
  [[ $XMAX =~ ^[0-9]+$ ]]           || echoxit "incorrect value (option -X): $XMAX" ; 
fi
if [ "$XMIN" != "$NA" ] && [ "$XMAX" != "$NA" ]
then
  [ $XMIN -ge $XMAX ]               && echoxit "xmin (option -x) should be smaller than xmax (option -X)" ; 
  [ $(( $XMAX - $XMIN )) -lt 1000 ] && echoxit "xmax-xmin should be at least 1000 (options -X and -x)" ; 
fi
if [ "$YMIN" != "$NA" ]
then
  [[ $YMIN =~ ^[0-9]+$ ]]           || echoxit "incorrect value (option -y): $YMIN" ;
  [ $YMIN -ge 100 ]                 && echoxit "ymin should be smaller than 100 (option -y)" ; 
fi
if [ "$YMAX" != "$NA" ]
then
  [[ $YMAX =~ ^[0-9]+$ ]]           || echoxit "incorrect value (option -Y): $YMAX" ; 
  [ $YMAX -le 0 ]                   && echoxit "ymax should be higher than 0 (option -Y)" ; 
fi
if [ "$YMIN" != "$NA" ] && [ "$YMAX" != "$NA" ]
then
  [ $YMIN -ge $YMAX ]               && echoxit "ymin (option -y) should be smaller than ymax (option -Y)" ; 
  [ $(( $YMAX - $YMIN )) -lt 10 ]   && echoxit "ymax-ymin should be at least 10 (options -Y and -y)" ; 
fi
[[ $DOTSPAN =~ ^[0-9]+\.[0-9]+$ ]]  || echoxit "incorrect value (option -d): $DOTSPAN" ;
[[ $RATIO =~ ^[0-9]+\.[0-9]+$ ]]    || echoxit "incorrect value (option -a): $RATIO" ;    
[[ $NTHREADS =~ ^[0-9]+$ ]]         || echoxit "incorrect value (option -t): $NTHREADS" ; 

shift "$(( $OPTIND - 1 ))"

echo ;

## tmp files #################################################################################################
DB=$(randfile $OUTFILE.db);
FRAG=$(randfile $OUTFILE.frag);
TMP=$(randfile $OUTFILE.tmp);

## defining traps ############################################################################################
finalizer() {
  rm -f $DB $DB.ndb $DB.nhr $DB.nin $DB.njs $DB.not $DB.nsq $DB.ntf $DB.nto ;
  rm -f $FRAG $TMP ;
}
echoxit() {
  finalizer ;
  echo -e "\n$1" >&2 ;
  exit 1 ;
}    
trap 'finalizer;exit 1'  INT ERR TERM ;


##############################################################################################################
####                                                                                                      ####
#### PROCESSING REFFILE                                                                                   ####  
####                                                                                                      ####
##############################################################################################################

echo " reference file   (-r)  $REFFILE" >&2 ;

## formatting input sequences ################################################################################
RLGT=$(grep -v "^>" $REFFILE | tr -d ' [:cntrl:]' | wc -c);

echo " reference length       $RLGT" ;

## setting xmin, xmax, ymin, ymax ############################################################################
[ "$XMIN" == "$NA" ] && XMIN=0;
[ "$XMAX" == "$NA" ] && XMAX=$RLGT;

echo " xmin             (-x)  $XMIN" ;
echo " xmax             (-X)  $XMAX" ;

## setting window size #######################################################################################
if [ "$WSIZE" == "$NA" ]
then
  WSIZE=$(( ( $XMAX - $XMIN ) / 1000 ));
  if [ $WSIZE -lt 50 ]
  then
    WSIZE=30;
    [ $RLGT -gt 50000  ] && WSIZE=35;
    [ $RLGT -gt 99999  ] && WSIZE=40;
  fi
  WSIZE=$($GAWK -v x=$WSIZE 'BEGIN{--x;while(++x){srx=sqrt(x);y=1;while((++y<=srx)&&(x%y!=0)){}if(y>srx)break;}print x;}');
fi

echo " window size      (-w)  $WSIZE" ;

## formatting reference sequences ############################################################################
echo ">ref"            > $FRAG ;
grep -v "^>" $REFFILE >> $FRAG ;
$MKBNDB -in $FRAG -out $DB &>/dev/null ;

## checking no. threads ######################################################################################
[ $NTHREADS -lt 1 ] && NTHREADS=1;
[ $NTHREADS -gt 1 ] && BLASTN="$BLASTN -num_threads $NTHREADS -mt_mode 1";

echo " no. threads      (-t)  $NTHREADS" ;


##############################################################################################################
####                                                                                                      ####
#### PROCESSING INFILES                                                                                   ####  
####                                                                                                      ####
##############################################################################################################
INFOFILES="";
N=0;
for FASTA in $@
do
  if [ ! -e $FASTA ]; then echo "file not found: $FASTA"     >&2 ; continue; fi 
  if [   -d $FASTA ]; then echo "not a file: $FASTA"         >&2 ; continue; fi 
  if [ ! -s $FASTA ]; then echo "empty file: $FASTA"         >&2 ; continue; fi 
  if [ ! -r $FASTA ]; then echo "no read permission: $FASTA" >&2 ; continue; fi

  N=$(( $N + 1 ));

  echo " infile $N               $FASTA" ;

  INFO=$(randfile $FASTA.info);
  INFOFILES="$INFOFILES $INFO";
  finalizer() {
    rm -f $DB $DB.ndb $DB.nhr $DB.nin $DB.njs $DB.not $DB.nsq $DB.ntf $DB.nto ;
    rm -f $FRAG $TMP ;
    rm -f $INFOFILES ;
  }
  
  ## cutting REFERENCE into ws-long fragments ################################################################
  #  + >x  where  x = 1000000000 + frag number
  hw=$(( $WSIZE / 2 ));
  $GAWK '/^>/{if(++n>1)printf"@";next}{printf$0}' $FASTA |
    tr -d ' ' |                                       # discarding blanks (if any)
      tr '[a-z]' '[A-Z]' |                            # uppercasing
        fold -w $hw |                                 # wrapping every WSIZE/2 nucleotides
          $GAWK -v hw=$hw 'BEGIN  {x=1000000000}
                           (NR==1){fpre=$1;next}
                                  {fcur=$1;
                                   frag=fpre""fcur;
                                   if(length(frag)>=hw){
                                     print">"(++x)"\t"frag;
                                   }
                                   fpre=fcur;
                                  }' |
            grep -v "@" |
              $TAWK '{print$1;print$2}' > $FRAG ;

  ## first blastn similarity search ##########################################################################
  KMER=7;
  [ $WSIZE -gt 100  ] && KMER=11;
  [ $WSIZE -gt 500  ] && KMER=15;
  [ $WSIZE -gt 1000 ] && KMER=21;
  [ $WSIZE -gt 5000 ] && KMER=25;
  #                                                   output fields: 1----- 2--- 3----- 4--- 5----- 6----- 7--- 8----- 
  $BLASTN -query $FRAG -db $DB -word_size $KMER -out $TMP -outfmt '6 qseqid qlen qstart qend sseqid sstart send pident' 2>/dev/null ;

  ## second blastn similarity search for putative divergent fragments ########################################
  if [ $KMER -gt 11 ]
  then
    ## selecting blastn hits with query aligned fraction s > 90% ###################################
    $TAWK '($1==q){next} 
                  {q=$1;
                   if((s=100*($4-$3+1)/$2)<90)next;
                   print $1"\t"int(($6+$7)/2)"\t"s"\t"$8}' $TMP > $INFO ;          # 1:q 2:x 3:s 4:y

    ## getting selected queries ####################################################################
    $TAWK '{print">"$1}' $INFO > $TMP ;
    paste - - < $FRAG |
      grep -v -F -f $TMP |
        $TAWK '{print$1;print$2}' > $FRAG.bis ;                               # grep -c "^>" $FRAG ;
    mv $FRAG.bis $FRAG ;                                                      # grep -c "^>" $FRAG ;
    
    ## saving selected blastn hit results ##########################################################
    $TAWK '{print$2"\t"$3"\t"$4}' $INFO > $INFO.bis ;
    mv $INFO.bis $INFO ;                                                               # 1:x 2:s 3:y
  
    ## second blastn similarity search based on small word sizes ###################################
    KMER=7;
    [ $WSIZE -gt 100  ] && KMER=11;
    #                                                   output fields: 1----- 2--- 3----- 4--- 5----- 6----- 7--- 8----- 
    $BLASTN -query $FRAG -db $DB -word_size $KMER -out $TMP -outfmt '6 qseqid qlen qstart qend sseqid sstart send pident' 2>/dev/null ;
  fi
  
  ## summarizing BLAST hits ##################################################################################
  $TAWK '($1==q){next} 
                {q=$1;
                 print int(($6+$7)/2)"\t"(100*($4-$3+1)/$2)"\t"$8}' $TMP >> $INFO ;
  sort -k1g,1 -k3rg,3 -k2rg,2 $INFO |
    $TAWK '($1==q){next}
                  {q=$1;print}' > $TMP ;
  mv $TMP $INFO ; touch $TMP ;
  
  ## smoothing ###############################################################################################
  #
  #  for each x_i, replacing the similarity y_i with Y_i = S_i / N_i
  #    where: S_i = w_{i,i-3}*y_{i-3} + ... + w_{i,i}*y_i + ... + w_{i,i+3}*y_{i+3}
  #           N_i = w_{i,i-3}         + ... + w_{i,i}     + ... + w_{i,i+3}
  #  Y_i is therefore a weigthed average of the similarities y_{i-3} ... y_{i+3}
  #
  #  for each x_i, replacing the aligned fraction s_i with Z_i = T_i / M_i
  #    where: T_i = W_{i,i-3}*s_{i-3} + ... + W_{i,i}*s_i + ... + W_{i,i+3}*s_{i+3}
  #           M_i = W_{i,i-3}         + ... + W_{i,i}     + ... + W_{i,i+3}
  #  Z_i is therefore a weighted average of the fractions s_{i-3} ... s_{i+3}
  #
  #  the two weigths are computed as follows:
  #    W_{i,j} = max[0, 1 + (alpha-1) * |x_i - x_j| / ws],  where 0 < alpha < 1 is a fixed constant
  #    w_{i,j} = s_j * W_{i,j} 
  #  W_{i,j} is a decreasing linear function on |x_i - x_j|, therefore as close to 1 as x_j is close to x_i
  #  the negative slope of W_{i,j} is depending on alpha; here, alpha = 0.75, therefore leading to:
  #
  #    |x_i - x_j|   W_{i,j}
  #    -----------   -------
  #    0             1.000
  #    ws/2          0.875
  #    ws            0.750 
  #    2*ws          0.500
  #    3*ws          0.250
  #    >=4*ws        0.000   
  #
  if $SMOOTH
  then
    $TAWK -v ws=$WSIZE 'BEGIN  {alpha=0.75;a=(alpha-1)/ws;}
                        (NR==1){x1=$1;s1=$2;y1=$3;next} 
                        (NR==2){x2=$1;s2=$2;y2=$3;next}
                        (NR==3){x3=$1;s3=$2;y3=$3;next}
                        (NR==4){x4=$1;s4=$2;y4=$3;next}
                        (NR==5){x5=$1;s5=$2;y5=$3;next}
                        (NR==6){x6=$1;s6=$2;y6=$3;next}
                               {x7=$1;s7=$2;y7=$3;
                                w1=a*(x4-x1)+1;(w1<0)&&w1=0;ss =w1*s1;ns =w1;w1*=s1;sy =w1*y1;ny =w1;
                                w2=a*(x4-x2)+1;(w2<0)&&w2=0;ss+=w2*s2;ns+=w2;w2*=s2;sy+=w2*y2;ny+=w2;
                                w3=a*(x4-x3)+1;(w3<0)&&w3=0;ss+=w3*s3;ns+=w3;w3*=s3;sy+=w3*y3;ny+=w3;
                                                            ss+=   s4;ns++;  w4 =s4;sy+=w4*y4;ny+=w4;
                                w5=a*(x5-x4)+1;(w5<0)&&w5=0;ss+=w5*s5;ns+=w5;w5*=s5;sy+=w5*y5;ny+=w5;
                                w6=a*(x6-x4)+1;(w6<0)&&w6=0;ss+=w6*s6;ns+=w6;w6*=s6;sy+=w6*y6;ny+=w6;
                                w7=a*(x7-x4)+1;(w7<0)&&w7=0;ss+=w7*s7;ns+=w7;w7*=s7;sy+=w7*y7;ny+=w7;
                                print x4"\t"(ss/ns)"\t"(sy/ny);                           
                                x1=x2;s1=s2;y1=y2;
                                x2=x3;s2=s3;y2=y3;
                                x3=x4;s3=s4;y3=y4;
                                x4=x5;s4=s5;y4=y5;
                                x5=x6;s5=s6;y5=y6;
                                x6=x7;s6=s7;y6=y7;
                               }' $INFO > $TMP ;
    mv $TMP $INFO ;
    touch $TMP ;
  fi
done


##############################################################################################################
####                                                                                                      ####
#### WRITING SVG FILE                                                                                     ####  
####                                                                                                      ####
##############################################################################################################

## setting ymin, ymax ########################################################################################
[ "$YMIN" == "$NA" ] && YMIN=0;
[ "$YMAX" == "$NA" ] && YMAX=100;

echo " ymin             (-y)  $YMIN" ;
echo " ymax             (-Y)  $YMAX" ;

## estimating dimensions #####################################################################################
ZOOM=$(bc -l <<<"scale=5;5/$DOTSPAN" | sed 's/^\./0\./');

echo " dot size factor  (-d)  $DOTSPAN" ;

WIDTH=$(bc <<<"($ZOOM*($XMAX-$XMIN)/$WSIZE+0.5)/1");
HEIGTH=$(bc <<<"(100*($YMAX-$YMIN)+0.5)/1");
WSPAN=1.0;
HSPAN=$(bc -l <<<"$WIDTH/($RATIO*$HEIGTH)");                 # heigth rescale factor; constraint: WSPAN*WIDTH/(HSPAN*HEIGTH)=RATIO
FONTSIZE=$(bc <<<"(sqrt($RATIO)*$HEIGTH/60+0.5)/1");         # font size
SFONTSIZE=$(bc <<<"($HSPAN*$FONTSIZE+0.5)/1");               # rescaled font size
LBORD=$(bc <<<"(6*$ZOOM*($XMAX-$XMIN)/(100*$WSIZE)+0.5)/1"); # left margin (6% x-axis lgt)
RBORD=$(( $LBORD / 2 ));                                     # right margin
UBORD=$(bc -l <<<"($LBORD/(3*$HSPAN)+0.5)/1");               # up margin
BBORD=$UBORD;                                                # bottom margin
WIDTH=$(bc <<<"($WSPAN*($LBORD+$ZOOM*($XMAX-$XMIN)/$WSIZE+$RBORD)+0.5)/1");
HEIGTH=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN)+50/$HSPAN+1.5*$FONTSIZE+2*($N+1)*(5/4)*$FONTSIZE+$BBORD)+0.5)/1");

echo " aspect ratio     (-a)  $RATIO" ;

## shrinking the whole file for web browsers #################################################################
GSCALE=$(bc -l <<<"scale=3;2000/$WIDTH" | sed 's/^\./0\./');
GWIDTH=$(bc <<<"($WIDTH*$GSCALE+0.5)/1");
GHEIGTH=$(bc <<<"($HEIGTH*$GSCALE+0.5)/1");

## setting parameters to gawk ################################################################################
TAWK="$TAWK -v lbord=$LBORD -v ubord=$UBORD -v wsize=$WSIZE -v wspan=$WSPAN -v hspan=$HSPAN -v zoom=$ZOOM";
TAWK="$TAWK -v xmin=$XMIN -v xmax=$XMAX -v ymin=$YMIN -v ymax=$YMAX";

## writing svg file ##########################################################################################
{ echo -e "<svg version=\"1.1\" width=\"$GWIDTH\" height=\"$GHEIGTH\" xmlns=\"http://www.w3.org/2000/svg\">\n" ;

  echo -e " <style>" ;
  echo -e "  text {" ;
  echo -e "   font-family: Arial, Helvetica, sans-serif;" ;
  echo -e "   fill: black;" ;
  echo -e "  }" ;
  echo -e "  line {" ;
  echo -e "   stroke: black;" ;
  echo -e "   stroke-linecap: round;" ;
  echo -e "   stroke-width: $(( 1 + $SFONTSIZE / 15 ));" ;
  echo -e "  }" ;
  echo -e "  circle {" ;
  echo -e "   stroke-width: 0;" ;
  echo -e "  }" ;
  echo -e " </style>\n" ;
  
  echo -e " <g transform=\"scale($GSCALE)\">\n" ;

  echo -e "  <!-- background -->\n" ;

  echo -e "  <rect width=\"$WIDTH\" height=\"$HEIGTH\" fill=\"white\"/>\n" ;

  echo -e "  <!-- x-axis -->\n" ;

  x1=$(bc <<<"($WSPAN*$LBORD+0.5)/1");                              # x0
  y1=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN))+0.5)/1");          # y0
  x2=$(bc <<<"($WSPAN*($LBORD+$ZOOM*($XMAX-$XMIN)/$WSIZE)+0.5)/1"); # x-axis lgt = ZOOM*(XMAX-XMIN)/WSIZE
  y2=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN))+0.5)/1");
  echo -e "  <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\"/>\n" ;
  x=0;
  step=$(( ( $XMAX - $XMIN ) / 9 ));
  [ $(( $XMAX - $XMIN )) -gt 10000 ]   && step=$(( 1000 * ( $step / 1000 ) ));
  [ $(( $XMAX - $XMIN )) -gt 100000 ]  && step=$(( 10000 * ( $step / 10000 ) ));
  [ $(( $XMAX - $XMIN )) -gt 1000000 ] && step=$(( 100000 * ( $step / 100000 ) ));
  for p in $(seq $XMIN $step $XMAX)
  do
    x1=$(bc <<<"($WSPAN*($LBORD+$ZOOM*$x/$WSIZE)+0.5)/1");
    y1=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN)+50/$HSPAN)+0.5)/1");
    x2=$(bc <<<"($WSPAN*($LBORD+$ZOOM*$x/$WSIZE)+0.5)/1");
    y2=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN))+0.5)/1");
    echo -e "  <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\"/>\n" ;
    x1=$(bc <<<"($WSPAN*($LBORD+$ZOOM*$x/$WSIZE)+0.5)/1");
    y1=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN)+50/$HSPAN+1.2*$FONTSIZE)+0.5)/1");
    echo -e "  <text x=\"$x1\" y=\"$y1\" font-size=\"$SFONTSIZE\" text-anchor=\"middle\">$p</text>\n";
    x=$(( $x + $step ));
  done
  x=0;
  step=$(( $step / 10 ));
  for p in $(seq $XMIN $step $XMAX)
  do
    x1=$(bc <<<"($WSPAN*($LBORD+$ZOOM*$x/$WSIZE)+0.5)/1");
    y1=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN)+25/$HSPAN)+0.5)/1");
    x2=$(bc <<<"($WSPAN*($LBORD+$ZOOM*$x/$WSIZE)+0.5)/1");
    y2=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN))+0.5)/1");
    echo -e "  <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\"/>\n" ;
    x=$(( $x + $step ));
  done

  echo -e "  <!-- y-axis -->\n" ;

  y=$(( $YMAX - $YMIN ));
  x1=$(bc <<<"($WSPAN*$LBORD+0.5)/1");               # x0
  y1=$(bc <<<"($HSPAN*($UBORD+100*$y)+0.5)/1");      # y0, y-axis lgt = 100*(YMAX-YMIN)
  x2=$(bc <<<"($WSPAN*$LBORD+0.5)/1");
  y2=$(bc <<<"($HSPAN*($UBORD+0.5))/1");
  echo -e "  <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\"/>\n" ;
  step=10;
  [ $(( $YMAX - $YMIN )) -le 50 ]  && step=5;
  [ $(( $YMAX - $YMIN )) -le 20 ]  && step=2;
  [ $(( $YMAX - $YMIN )) -le 10 ]  && step=1;
  for p in $(seq $YMIN $step $YMAX)
  do
    x1=$(bc <<<"($WSPAN*($LBORD-50)+0.5)/1");
    y1=$(bc <<<"($HSPAN*($UBORD+100*$y)+0.5)/1");  
    x2=$(bc <<<"($WSPAN*($LBORD)+0.5)/1");
    y2=$(bc <<<"($HSPAN*($UBORD+100*$y)+0.5)/1");
    echo -e "  <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\"/>\n" ;
    x1=$(bc <<<"($WSPAN*($LBORD-50)+0.5)/1");
    y1=$(bc <<<"($HSPAN*($UBORD+100*$y+$FONTSIZE/3)+0.5)/1");  
    echo -e "  <text x=\"$x1\" y=\"$y1\" font-size=\"$SFONTSIZE\" text-anchor=\"end\">$p%&#160;</text>\n";
    y=$(( $y - $step ));
  done
  
  echo -e "  <!-- dots -->\n" ;

  i=0;
  for f in $INFOFILES
  do
    $TAWK -v col=${COLORS[$i]} '($1<xmin){next}
                                ($1>xmax){next}
                                ($3<ymin){next}
                                ($3>ymax){next}
                                         {cx=int(wspan*(lbord+zoom*($1-xmin)/wsize)+0.5);
                                          cy=int(hspan*(ubord+100*(ymax-$3))+0.5);
                                          r=int($2/10+0.5);
                                          print "  <circle cx=\""cx"\" cy=\""cy"\" r=\""r"\" fill=\""col"\" fill-opacity=\"0.6\"/>\n";
                                         }' $f ;
    i=$(( $i + 1 ));
  done

  echo -e "  <!-- names -->\n" ;

  SFONTSIZE=$(( 5 * $SFONTSIZE / 4 ));
  name=$(basename ${REFFILE%.*});
  x1=$(bc <<<"($WSPAN*($LBORD)+0.5)/1");
  y1=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN)+50/$HSPAN+1.5*$FONTSIZE+2*(5/4)*$FONTSIZE)+0.5)/1");
  echo -e "  <text x=\"$x1\" y=\"$y1\" font-size=\"$SFONTSIZE\" text-anchor=\"start\">$name</text>\n";
  i=0;
  for f in $INFOFILES
  do
    cx=$(bc <<<"($WSPAN*($LBORD-$SFONTSIZE/2)+0.5)/1");
    cy=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN)+50/$HSPAN+1.5*$FONTSIZE+2*($i+2)*(5/4)*$FONTSIZE-$FONTSIZE/3)+0.5)/1");
    r=$(( $SFONTSIZE / 4 ));
    echo -e "  <circle cx=\"$cx\" cy=\"$cy\" r=\"$r\" fill=\"${COLORS[$i]}\" fill-opacity=\"0.6\"/>\n";
    cx=$(( $cx + 1 ));
    echo -e "  <circle cx=\"$cx\" cy=\"$cy\" r=\"$r\" fill=\"${COLORS[$i]}\" fill-opacity=\"0.6\"/>\n";
    name=$(basename ${f%.*});
    name=${name%.*};
    name=${name%.*};
    x1=$(bc <<<"($WSPAN*$LBORD+0.5)/1");
    y1=$(bc <<<"($HSPAN*($UBORD+100*($YMAX-$YMIN)+50/$HSPAN+1.5*$FONTSIZE+2*($i+2)*(5/4)*$FONTSIZE)+0.5)/1");
    echo -e "  <text x=\"$x1\" y=\"$y1\" font-size=\"$SFONTSIZE\" text-anchor=\"start\">$name</text>\n";
    i=$(( $i + 1 ));
  done
  
  echo -e " </g>\n" ;

  echo "</svg>" ; } > $OUTFILE ;

echo " output file      (-o)  $OUTFILE" ;

finalizer ;

echo ;

exit ; 
